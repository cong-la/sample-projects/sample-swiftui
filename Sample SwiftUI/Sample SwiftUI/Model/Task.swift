//
//  Task.swift
//  Sample SwiftUI
//
//  Created by Cong Thanh La on 07/01/2023.
//

import Foundation

struct Task {
    var id = UUID()
    var name: String = ""
}

extension Task {
    static func all() -> [Task] {
        return [
            Task(name: "Task 01"),
            Task(name: "Task 02"),
            Task(name: "Task 03"),
            Task(name: "Task 04"),
            Task(name: "Task 05"),
        ]
    }
}
