//
//  Sample_SwiftUIApp.swift
//  Sample SwiftUI
//
//  Created by Cong Thanh La on 07/01/2023.
//

import SwiftUI

@main
struct Sample_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
