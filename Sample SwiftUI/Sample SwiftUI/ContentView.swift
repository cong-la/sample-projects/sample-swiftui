//
//  ContentView.swift
//  Sample SwiftUI
//
//  Created by Cong Thanh La on 07/01/2023.
//

import SwiftUI

struct ContentView: View {
    
    let tasks = Task.all()
    
    var body: some View {
        List(self.tasks, id: \.id) {
            task in
            Text(task.name)
                .font(.title2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
